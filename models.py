from sqlalchemy import Column, Integer, Text, ForeignKey, BOOLEAN, Date
from sqlalchemy.orm import declarative_base, relationship

from database import engine


Base = declarative_base()


class UserSalary(Base):
    __tablename__ = 'UserSalaries'
    id = Column(Integer(), primary_key=True, nullable=False, autoincrement=True)
    salary = Column(Integer())
    increasing_date = Column(Date())
    user_id = Column(Integer(), ForeignKey('Users.id'), unique=True)

    def __repr__(self):
        return f'{self.user_id} {self.agency_id}'


class Role(Base):
    __tablename__ = 'Roles'
    id = Column(Integer(), primary_key=True, nullable=False, autoincrement=True)
    name = Column(Text(), nullable=False)

    def __repr__(self):
        return f'{self.name}'


class User(Base):
    __tablename__ = 'Users'
    id = Column(Integer(), primary_key=True, nullable=False, autoincrement=True)
    username = Column(Text(), nullable=False)
    fullname = Column(Text(), nullable=False)
    hashed_password = Column(Text(), nullable=False)
    role_id = Column(Integer(), ForeignKey('Roles.id'), default=1)
    is_active = Column(BOOLEAN, default=True, nullable=False)
    is_superuser = Column(BOOLEAN, default=False, nullable=False)
    is_verified = Column(BOOLEAN, default=False, nullable=False)

    def __repr__(self):
        return f'{self.username}'


if __name__ == '__main__':
    Base.metadata.create_all(engine, checkfirst=True)
